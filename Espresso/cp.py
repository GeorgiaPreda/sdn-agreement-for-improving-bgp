class CP:

    def __init__(self, filename):
        self.filename = filename
        self.reading = None
        self.traffic = {}
        self.destination_hosts = []
        self.egress_points = []
        self.readConfigSender()

    def readConfigSender(self):
        file = open(self.filename)
        row = file.readlines()
        for line in row:
            if line.find("Traffic") > -1:
                self.reading = "Traffic"
                continue
            if line.find("Egress") > -1:
                self.reading = "Egress"
                continue
            if line.find("Hosts prefixes") > -1:
                self.reading = "Hosts prefixes"
                continue
            if self.reading == "Traffic":
                parts = line.rstrip()
                parts = parts.split('-')
                self.traffic[parts[1]] = float(parts[0])
            if self.reading == "Egress":
                self.egress_points.append(line.rstrip())
            if self.reading == "Hosts prefixes":
                self.destination_hosts.append(line.rstrip())