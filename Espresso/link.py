class Link:
    def __init__(self, router1, router2, capacity, latency):
        self.router1 = router1
        self.router2 = router2
        self.capacity = capacity
        self.latency = latency
