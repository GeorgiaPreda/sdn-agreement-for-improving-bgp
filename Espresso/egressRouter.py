class EgressRouter:
    def __init__(self, router):
        self.traffic = {}
        self.router = router

    def assignMoreTraffic(self, host, trafficToHost):
        self.traffic[host] += trafficToHost
