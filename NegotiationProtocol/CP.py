from dataclasses import dataclass


@dataclass
class ConfigSender:
    file: str
    reading: str
    traffic: {}
    destination_hosts: []
    egress_points: []


@dataclass
class Traffic:
    traffic: float
    destination: str


def readConfigSender(filename, configSender):
    configSender.file = open(filename)
    row = configSender.file.readlines()
    for line in row:
        if line.find("Traffic") > -1:
            configSender.reading = "Traffic"
            continue
        if line.find("Egress") > -1:
            configSender.reading = "Egress"
            continue
        if line.find("Hosts prefixes") > -1:
            configSender.reading = "Hosts prefixes"
            continue
        if configSender.reading == "Traffic":
            parts = line.rstrip()
            parts = parts.split('-')
            configSender.traffic[parts[1]] = float(parts[0])
        if configSender.reading == "Egress":
            configSender.egress_points.append(line.rstrip())
        if configSender.reading == "Hosts prefixes":
            configSender.destination_hosts.append(line.rstrip())