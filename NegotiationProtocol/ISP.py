from dataclasses import dataclass


@dataclass
class ConfigReceiver:
    file: str
    reading: str
    routers: []
    ingress_points: []
    hosts: []
    links: []


@dataclass
class Link:
    router1: str
    router2: str
    capacity: float
    latency: float


@dataclass()
class Destination:
    host: str
    router: str


def readConfigReceiver(filename, configReceiver):
    file = open(filename)
    row = file.readlines()
    for line in row:
        if line.find("Topology") > -1:
            configReceiver.reading = "Topology"
            continue
        if line.find("Ingress") > -1:
            configReceiver.reading = "Ingress"
            continue
        if line.find("Hosts") > -1:
            configReceiver.reading = "Hosts"
            continue
        if line.find("Capacities") > -1:
            configReceiver.reading = "Capacities"
            continue
        if configReceiver.reading == "Topology":
            configReceiver.routers.append(line.rstrip())
        if configReceiver.reading == "Capacities":
            parts = line.rstrip()
            parts = parts.split(':')
            tr = parts[0].split("-")
            cl = parts[1].split("/")
            configReceiver.links.append(Link(tr[0], tr[1], float(cl[0]), float(cl[1])))
        if configReceiver.reading == "Ingress":
            configReceiver.ingress_points.append(line.rstrip())
        if configReceiver.reading == "Hosts":
            parts = line.rstrip()
            parts = parts.split('-')
            configReceiver.hosts.append(Destination(parts[0], parts[1]))