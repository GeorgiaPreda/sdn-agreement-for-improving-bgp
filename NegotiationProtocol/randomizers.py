import math
import random

from NegotiationProtocol.ISP import Destination

TOTAL_PERC_LINKS_TO_MODIFY = 100
FIRST_PERC_LINKS_TO_MODIFY = 25
LOWER_FIRST_PERC = 60
UPPER_FIRST_PERC = 80
SECOND_PERC_LINKS_TO_MODIFY = 10
LOWER_SECOND_PERC = 40
UPPER_SECOND_PERC = 59
THIRD_PERC_LINKS_TO_MODIFY = 20
LOWER_THIRD_PERC = 20
UPPER_THIRD_PERC = 39
FOURTH_PERC_LINKS_TO_MODIFY = 45
LOWER_FOURTH_PERC = 0
UPPER_FOURTH_PERS = 19

log_file = None


def setLogFile(log_file_name):
    global log_file
    log_file = open(log_file_name, "w")


def reduceLinksHelper(topology, x, currEl, noLinksModified, levelLinksToModify, lower_perc, upper_perc):
    inEl = currEl
    while currEl < inEl + int(levelLinksToModify * noLinksModified / 100):
        rand_boundaries = random.randrange(lower_perc, upper_perc)
        new_val_band = topology[x[currEl][0]][x[currEl][1]]['bandwidth'] - rand_boundaries * \
                       (topology[x[currEl][0]][x[currEl][1]]['bandwidth']) / 100
        log_file.write("Link {}->{} has been reduced from {} to {} ({} %) \n".format(x[currEl][0], x[currEl][1],
                        topology[x[currEl][0]][x[currEl][1]]['bandwidth'], new_val_band, rand_boundaries))
        topology[x[currEl][0]][x[currEl][1]]['bandwidth'] = new_val_band
        currEl += 1
    return currEl


def reduceLinksCapacity(topology):
    noEdges = len(topology.edges)
    noLinksModified = int(TOTAL_PERC_LINKS_TO_MODIFY * noEdges / 100)
    x = random.sample(topology.edges, noLinksModified)

    currEl = 0
    currEl = reduceLinksHelper(topology, x, currEl, noLinksModified, FIRST_PERC_LINKS_TO_MODIFY, LOWER_FIRST_PERC,
                               UPPER_FIRST_PERC)
    currEl = reduceLinksHelper(topology, x, currEl, noLinksModified, SECOND_PERC_LINKS_TO_MODIFY, LOWER_SECOND_PERC,
                               UPPER_SECOND_PERC)
    currEl = reduceLinksHelper(topology, x, currEl, noLinksModified, THIRD_PERC_LINKS_TO_MODIFY, LOWER_THIRD_PERC,
                               UPPER_THIRD_PERC)
    reduceLinksHelper(topology, x, currEl, noLinksModified, FOURTH_PERC_LINKS_TO_MODIFY, LOWER_FOURTH_PERC,
                      UPPER_FOURTH_PERS)


def randomizeIngressRouters(topology, configSender, configReceiver, percOfIngressRoutersISPToChoose):
    noNodes = len(topology.nodes)
    noIngressRouters = math.ceil(percOfIngressRoutersISPToChoose * noNodes / 100)
    newIngressRouters = random.sample(topology.nodes, noIngressRouters)
    if noIngressRouters > 0:
        configSender.egress_points.clear()
        configSender.egress_points = newIngressRouters
        configReceiver.ingress_points.clear()
        configReceiver.ingress_points = newIngressRouters
    log_file.write("New Ingress Routers ISP: {} \n".format(configReceiver.ingress_points))


def findMaximumLinkCapacity(topology):
    maxLinkCap = 0.0
    for link in topology.edges:
        maxLinkCap = max(maxLinkCap, topology[link[0]][link[1]]['bandwidth'])
    return maxLinkCap


def randomizeEgressRouters(topology, configSender, configReceiver, percOfEgressRoutersHostsToChoose):
    noNodes = len(topology.nodes)
    noEgressRouters = math.ceil(percOfEgressRoutersHostsToChoose * noNodes / 100)
    newEgressRouters = []
    if noEgressRouters > 0:
        while len(newEgressRouters) < noEgressRouters:
            newEgressPoint = random.sample(topology.nodes, 1)
            if newEgressPoint not in configSender.egress_points and newEgressPoint not in newEgressRouters and \
                    newEgressPoint not in configReceiver.ingress_points:
                newEgressRouters.append(newEgressPoint)
        configSender.destination_hosts.clear()
        configReceiver.hosts.clear()
        for i, point in enumerate(newEgressRouters):
            configReceiver.hosts.append(Destination(str(i) + "/8", point[0]))
            configSender.destination_hosts.append(str(i) + "/8")
    log_file.write("New Egress Routers Hosts: {} \n".format(configReceiver.hosts))


def assignTraffic(topology, configSender, traffic):
    traffic = traffic * findMaximumLinkCapacity(topology)
    noEgressRouters = len(configSender.destination_hosts)
    trafficPerEgress = traffic/noEgressRouters
    configSender.traffic.clear()
    for egress in configSender.destination_hosts:
        configSender.traffic[egress] = trafficPerEgress
    log_file.write("Attempted traffic to send: {} \n".format(configSender.traffic))


def randomizeTraffic(topology, configSender, traffic):
    traffic = traffic * findMaximumLinkCapacity(topology)
    trafficLeft = traffic
    configSender.traffic.clear()
    for i in range(0, len(configSender.destination_hosts)-1):
        rand = random.randrange(0, 100)
        newTraffic = rand * trafficLeft / 100
        configSender.traffic[configSender.destination_hosts[i]] = newTraffic
        trafficLeft -= newTraffic
    configSender.traffic[configSender.destination_hosts[len(configSender.destination_hosts)-1]] = trafficLeft
    log_file.write("New traffic: {} \n".format(configSender.traffic))

